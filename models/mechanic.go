package models

type Mechanics struct {
	ID         uint   `json:"id" gorm:"primary_key"`
	Name       string `json:"name"`
	LocationID uint
	Location   Locations `gorm:"foreignKey:LocationID"`
}
