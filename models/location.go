package models

type Locations struct {
	ID   uint   `json:"id" gorm:"primary_key"`
	Name string `json:"location_name" gorm:"unique"`
}
