package main

import (
	"gitlab.com/christopher.lim/go-rest-api/db"
	"gitlab.com/christopher.lim/go-rest-api/models"
	"gitlab.com/christopher.lim/go-rest-api/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	db.ConnectToDatabase()

	db.DB.AutoMigrate(
		&models.City{}, &models.User{}, &models.Locations{}, &models.Mechanics{})

	// Setup routes
	routes.SetupRouter(router)

	router.Run()
}
