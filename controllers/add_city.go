package controllers

import (
  "net/http"
  "gitlab.com/christopher.lim/go-rest-api/models"
  "gitlab.com/christopher.lim/go-rest-api/db"

  "github.com/gin-gonic/gin"
)

type addCityRequest struct {
  Name    string `json:"name" binding:"required"`
  Country string `json:"country" binding:"required"`
}

func AddCity(c *gin.Context) {
  // Validate request
  var req addCityRequest
  if err := c.ShouldBindJSON(&req); err != nil {
    c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
    return
  }

  // Add city
  city := models.City{
    Name:    req.Name,
    Country: req.Country,
  }
  db.DB.Create(&city)

  c.JSON(http.StatusCreated, gin.H{"data": city})
}