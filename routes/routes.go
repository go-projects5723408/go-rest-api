package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/christopher.lim/go-rest-api/controllers"
	"gitlab.com/christopher.lim/go-rest-api/middlewares"
)

func SetupRouter(router *gin.Engine) {
	router.POST("/api/auth/signup", controllers.CreateUser)
	router.POST("/api/auth/login", controllers.Login)
	// Protected
	authenticated := router.Group("/")
	authenticated.Use(middlewares.CheckAuth)
	authenticated.POST("/api/city", controllers.AddCity)
	authenticated.GET("/api/city/:id", controllers.GetCity)
	authenticated.DELETE("api/city/:id", controllers.RemoveCity)
	authenticated.GET("/api/user/profile", controllers.GetUserProfile)
}
