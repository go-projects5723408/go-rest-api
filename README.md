# Go Rest API With Gin, GORM, GORM's PostgresSQL and Docker

### Installation
- Go package
- PostgresSQl
- Docker

### Package Used
```
go get github.com/gin-gonic/gin
go get gorm.io/gorm
go get -u github.com/go-sql-driver/mysql
go get github.com/joho/godotenv
go install github.com/cosmtrek/air@latest
go get github.com/golang-jwt/jwt/v4
go get golang.org/x/crypto
```

### Create a .env with these environment variables
```
DB_HOST=
DB_USER=
DB_PASSWORD=
DB_NAME=
DB_PORT=
SECRET=
MYSQL_ROOT_PASSWORD=
MYSQL_DATABASE=
MYSQL_USER=
MYSQL_PASSWORD=
```


### TO Run the Service
To Run Docker:
```
docker-compose up --build -d
```

To Stop Docker:
```
docker-compose down
```

```
JWT: a Go library for JSON Web Token implementation.
DotEnv: a Go dependency for managing environment variables.
Gorm: an ORM (Object Relational Mapper) for Golang.
Go Crypto: a cryptography library in Go that will be used for password encryption and decryption.
Go Air: a Go package facilitating automatic app reloading upon saving changes.
```

```
go mod migrate/migrate.go
```